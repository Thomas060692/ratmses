﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class StaticCheckpointEvent
{
    public static event Action<Vector3> OnCheckpointChange;
    public static void CheckpointDesactivation(Vector3 g)
    {
        OnCheckpointChange?.Invoke(g);
    }
}
