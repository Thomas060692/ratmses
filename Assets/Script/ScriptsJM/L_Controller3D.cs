﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider))]

public class L_Controller3D : L_RaycastCtrl
{
    float maxClimbAngle = 80f;
    float maxDescendAngle = 80f;

    public CollisionInfo collisions;

    Vector3 Startpos;

    [SerializeField]
    GameObject
            MouseModel;

    public bool
        IsGround;

    public override void Start() {
        base.Start();
        collisions.faceDir = 1;

        IsGround = true;
    }

    public void Move(Vector3 velocity, bool standingOnPlatform = false) {
        UpdateRaycastOrigins();
        collisions.Reset();
        collisions.velocityOld = velocity;

        if (velocity.y < 0) {
            DescendSlope(ref velocity);
        }

        if (velocity.x != 0){
            collisions.faceDir = (int)Mathf.Sign(velocity.x);
        }

        HorizontalCollisions(ref velocity);

        if (velocity.y != 0) {
            VerticalCollisions(ref velocity);
        }

        if(standingOnPlatform == true) {
            collisions.below = true;
        }

        transform.Translate(new Vector3(0, velocity.y, 0));

        transform.RotateAround(targetCenter.position, Vector3.up, velocity.x * DistanceToAngles());
        

    }

    void HorizontalCollisions(ref Vector3 velocity) {
        float directionX = collisions.faceDir;
        float rayLenght = Mathf.Abs(velocity.x/6) + skinWidth;

        if (velocity.x < skinWidth) {
            rayLenght = 2 * skinWidth;
        }
    
        for (int i = 0; i < horizontalRayCount; i++) {
            Vector3 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft: raycastOrigins.bottomRight;
            rayOrigin += transform.up * (horizontalRaySpacing * i);
            RaycastHit hit;
            var horizontalHit = Physics.Raycast(rayOrigin, transform.right * directionX, out hit, rayLenght, collisionMask);

            Debug.DrawRay(rayOrigin, transform.right * directionX * rayLenght, Color.green);

            if (horizontalHit) {

                if(hit.distance == 0) {
                    continue;
                }

                float slopeAngle = Vector3.Angle(hit.normal, transform.up);

                if (i == 0 && slopeAngle <= maxClimbAngle) {
                    if (collisions.descendingSlope) {
                        collisions.descendingSlope = false;
                        velocity = collisions.velocityOld;
                    }

                    float distanceToSlopeStart = 0;
                    if (slopeAngle!= collisions.slopeAngleOld) {
                        distanceToSlopeStart = hit.distance - skinWidth;
                        velocity.x -= distanceToSlopeStart * directionX;
                    }
                    ClimbSlope(ref velocity, slopeAngle);
                    velocity.x += distanceToSlopeStart * directionX;
                }

                if (!collisions.climbingSlope || slopeAngle > maxClimbAngle) {
                    velocity.x = (hit.distance - skinWidth) * directionX;
                    rayLenght = hit.distance;

                    if (collisions.climbingSlope) {
                        velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad * Mathf.Abs(velocity.x));
                    }
                    collisions.left = directionX == -1;
                    collisions.right = directionX == 1;
                }
            }
        }
    }

    void ClimbSlope(ref Vector3 velocity, float slopeAngle) {
        float moveDistance = Mathf.Abs(velocity.x);
        float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (velocity.y <= climbVelocityY) {
            velocity.y = climbVelocityY;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);

            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
        }

        Debug.DrawRay(transform.position + new Vector3(0, 0.1f, 0), transform.up * velocity.y * 5, Color.blue);
        Debug.DrawRay(transform.position + new Vector3(0, 0.1f, 0), transform.right * velocity.x * 5, Color.blue);
    }

    void DescendSlope(ref Vector3 velocity) {
        float directionX = Mathf.Sign(velocity.x);
        Vector3 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
        RaycastHit hit;
        var descendHit = Physics.Raycast(rayOrigin, -Vector2.up, out hit, Mathf.Infinity, collisionMask);

        if (descendHit) {
            float slopeAngle = Vector3.Angle(hit.normal, Vector3.up);
            if (slopeAngle != 0 && slopeAngle <= maxDescendAngle) {
                if(Mathf.Sign(hit.normal.x) == directionX) {
                    if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x)) {
                        float moveDistance = Mathf.Abs(velocity.x);
                        float descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                        velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
                        velocity.y -= descendVelocityY;

                        collisions.slopeAngle = slopeAngle;
                        collisions.descendingSlope = true;
                        collisions.below = true;
                    }
                }
            }
        }
    }

    void VerticalCollisions(ref Vector3 velocity) {
        float directionY = Mathf.Sign(velocity.y);
        float rayLenght = Mathf.Abs(velocity.y) + skinWidth;

        bool is_ground = false;

        for (int i = 0; i < verticalRayCount; i++) {
            Vector3 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft: raycastOrigins.topLeft;
            rayOrigin += transform.right * (verticalRaySpacing * i + velocity.x);
            RaycastHit hit;
            var verticalHit = Physics.Raycast(rayOrigin, transform.up * directionY, out hit, rayLenght, collisionMask);

            Debug.DrawRay(rayOrigin, transform.up * directionY * rayLenght, Color.green);

            if (verticalHit) {

                is_ground = true;
                
                velocity.y = (hit.distance - skinWidth) * directionY;
                rayLenght = hit.distance;

                if (collisions.climbingSlope) {
                    velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
                }

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;

                if (collisions.climbingSlope) {
                    float directionX = Mathf.Sign(velocity.x);
                    rayLenght = Mathf.Abs(velocity.x + skinWidth);
                    rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + transform.up * velocity.y;
                    RaycastHit hitSecond;
                    var verticalHitSecond = Physics.Raycast(rayOrigin, transform.right * directionX, out hitSecond, rayLenght, collisionMask);

                    if (verticalHitSecond) {
                        float slopeangle = Vector3.Angle(hitSecond.normal, Vector3.up);
                        if (slopeangle != collisions.slopeAngle) {
                            velocity.x = (hitSecond.distance - skinWidth) * directionX;
                            collisions.slopeAngle = slopeangle;
                        }
                    }
                }
            }
        }

        if (is_ground)
        {
            if (IsGround)
            {

            }
            else if (!IsGround)
            {
                MouseModel.GetComponent<AnimationCharacter>().TriggerLanding();
                IsGround = true;   
            }
        }
        else if (!is_ground)
        {
            if (IsGround)
            {
                MouseModel.GetComponent<AnimationCharacter>().TriggerFalling();
                IsGround = false;

                if (this.GetComponent<L_Player>().WallJump)
                {
                    this.GetComponent<L_Player>().WallJump = false;
                }
            }
            else if (!IsGround)
            {
               
            }
        }
    }

    public struct CollisionInfo {
        public bool above, below;
        public bool left, right;

        public bool climbingSlope;
        public bool descendingSlope;
        public float slopeAngle, slopeAngleOld;
        public Vector3 velocityOld;
        public int faceDir;

        public void Reset() {
            above = below = false;
            left = right = false;
            climbingSlope = false;
            descendingSlope = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }
}