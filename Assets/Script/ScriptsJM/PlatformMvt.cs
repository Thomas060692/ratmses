﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMvt : MonoBehaviour
{
    public bool movementVertical = false;
    public bool movementHorizontal = false;
    public float horizontalMin = 0f;
    float horizontalMinAngles = 0f;
    public float horizontalMax = 3f;
    float horizontalMaxAngles = 0f;
    public float verticalMin = 0f;
    float verticalMinConverted = 0f;
    public float verticalMax = 3f;
    float verticalMaxConverted = 0f;
    public float horizontalSpeed = 1f;
    float horizontalSpeedConverted = 0f;
    public float verticalSpeed = 1f;
    float verticalSpeedConverted = 0f;
    bool horizontalPositive = true;
    bool verticalPositive = true;
    float horizontalPos = 0;
    float verticalPos = 0;

    void Start()
    {
        CalculateAngles();
    }
    void Update()
    {
        CalculateAngles();
        MovePlatform();
    }

    void CalculateAngles()
    {
        horizontalSpeedConverted = horizontalSpeed * 6;
        verticalSpeedConverted = verticalSpeed * 0.1f;
        horizontalMinAngles = horizontalMin * 6;
        horizontalMaxAngles = (-1 + horizontalMax) * 6;
        verticalMinConverted = verticalMin * 0.1f;
        verticalMaxConverted = (-1 + verticalMax) * 0.1f;
    }

    public void MovePlatform()
    {
        if (movementVertical == true)
        {
            MovePlatformVertical();
        }
        if (movementHorizontal == true)
        {
            MovePlatformHorizontal();
        }
    }

    void MovePlatformHorizontal()
    {
        if (movementHorizontal == true)
        {
            if (horizontalPositive == true)
            {
                transform.RotateAround(Vector3.zero, Vector3.up, horizontalSpeedConverted * Time.deltaTime);
                horizontalPos += horizontalSpeedConverted * Time.deltaTime;
            }
            else
            {
                transform.RotateAround(Vector3.zero, Vector3.up, horizontalSpeedConverted * Time.deltaTime * -1);
                horizontalPos -= horizontalSpeedConverted * Time.deltaTime;
            }

            if (horizontalPos >= horizontalMaxAngles)
            {
                horizontalPositive = false;
            }
            if (horizontalPos <= horizontalMinAngles)
            {
                horizontalPositive = true;
            }
        }
    }

    void MovePlatformVertical()
    {
        if (movementVertical == true)
        {
            if (verticalPositive == true)
            {
                transform.Translate(Vector3.up * Time.deltaTime * verticalSpeedConverted);
                verticalPos += verticalSpeedConverted * Time.deltaTime;
            }
            else
            {
                transform.Translate(Vector3.up * Time.deltaTime * verticalSpeedConverted * -1);
                verticalPos -= verticalSpeedConverted * Time.deltaTime;
            }

            if(verticalPos >= verticalMaxConverted)
            {
                verticalPositive = false;
            }
            if(verticalPos <= verticalMinConverted)
            {
                verticalPositive = true;
            }
        }
    }
}
