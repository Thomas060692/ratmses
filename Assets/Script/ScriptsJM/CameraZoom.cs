﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    public float zoomSpeed = 1f;
    float zoomFactor = 0;
    public float maxZoom = 500f;
    void Update()
    {
        if (Input.GetKey(KeyCode.E) && Input.GetKey(KeyCode.LeftShift) && zoomFactor <= maxZoom)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * zoomSpeed);
            zoomFactor += zoomSpeed;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift) && zoomFactor >= -maxZoom)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * -zoomSpeed);
            zoomFactor += -zoomSpeed;
        }
    }
}
