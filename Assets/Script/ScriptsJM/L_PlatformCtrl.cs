﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L_PlatformCtrl : L_RaycastCtrl
{
    public LayerMask passengerMask;

    List<PassengerMovement> passengerMovement;
    Dictionary<Transform, L_Controller3D> passengerDictionary = new Dictionary<Transform, L_Controller3D>();

    public float timeToGetThere = 5f;

    public bool mvtVertical = false;
    public int verticalDistance;

    float verticalUnits;
    float verticalSpeed = 0f;
    float verticalPos = 0f;
    bool verticalPositive = true;

    public bool mvtHorizontal = false;
    public int horizontalDistance;

    float horizontalUnits;
    float horizontalSpeed = 0f;
    float horizontalPos = 0f;
    bool horizPositive = true;

    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        CalculateTime();
        UpdateRaycastOrigins();
        Vector3 velocity = CalculatePlatformMovement();
        CalculatePassengerMvt(velocity);

        MovePassengers(true);
        MovePlatform(velocity);
        MovePassengers(false);
    }

    void CalculateTime()
    {
        if (verticalDistance > 0) { verticalUnits = verticalDistance - 1f; }
        if (verticalDistance < 0) { verticalUnits = verticalDistance + 1f; }
        if (verticalDistance == 0) { verticalUnits = verticalDistance; }
        if (horizontalDistance > 0) { horizontalUnits = horizontalDistance - 1f; }
        if (horizontalDistance < 0) { horizontalUnits = horizontalDistance + 1f; }
        if (horizontalDistance == 0) { horizontalUnits = horizontalDistance; }

        horizontalSpeed = horizontalUnits * 6 / (DistanceToAngles()) / timeToGetThere;
        verticalSpeed = verticalUnits / timeToGetThere * 0.1f;
    }

    Vector3 CalculatePlatformMovement() {
        Vector3 velocity = Vector3.zero;

        if (mvtHorizontal) {
            if (horizPositive) {
                velocity.x = horizontalSpeed * Time.deltaTime;
                horizontalPos += velocity.x * DistanceToAngles() / 6;
            }
            if (horizPositive == false) {
                velocity.x = horizontalSpeed * -1 * Time.deltaTime;
                horizontalPos += velocity.x * DistanceToAngles() / 6;
            }
            if (horizontalPos >= horizontalUnits) {
                horizPositive = false;
            }
            if (horizontalPos <= 0) {
                horizPositive = true;
            }
        }

        if (mvtVertical) {
            if (verticalPositive) {
                velocity.y = verticalSpeed * Time.deltaTime;
                verticalPos += velocity.y * 10;
            }
            if(verticalPositive == false) {
                velocity.y = verticalSpeed * Time.deltaTime * -1;
                verticalPos += velocity.y * 10;
            }

            if (verticalPos >= verticalUnits) {
                verticalPositive = false;
            }
            if (verticalPos <= 0) {
                verticalPositive = true;
            }
        }
        return velocity;
    }

    void MovePlatform(Vector3 velocity) {
        transform.Translate(transform.up * velocity.y);
        transform.RotateAround(targetCenter.position, Vector3.up, velocity.x * DistanceToAngles());
    }

    void MovePassengers(bool beforeMovePlatform) {
        foreach(PassengerMovement passenger in passengerMovement)
        {
            if (!passengerDictionary.ContainsKey(passenger.transform))
            {
                passengerDictionary.Add(passenger.transform, passenger.transform.GetComponent<L_Controller3D>());
            }
            if(passenger.moveBeforePlatform == beforeMovePlatform)
            {
                passengerDictionary[passenger.transform].Move(passenger.velocity, passenger.standingOnPlatform);
            }
        }
    }

    void CalculatePassengerMvt(Vector3 velocity) {
        HashSet<Transform> movedPassengers = new HashSet<Transform>();

        passengerMovement = new List<PassengerMovement> ();

        float directionX = Mathf.Sign(velocity.x);
        float directionY = Mathf.Sign(velocity.y);

        // Vertical moving platform
        if (velocity.y != 0) {
            float rayLenght = Mathf.Abs(velocity.y) + skinWidth;

            for (int i = 0; i < verticalRayCount; i++) {
                Vector3 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += transform.right * (verticalRaySpacing * i);
                RaycastHit hit;
                var hitBool = Physics.Raycast(rayOrigin, transform.up * directionY, out hit, rayLenght, passengerMask);

                Debug.DrawRay(rayOrigin, transform.up * directionY * rayLenght, Color.red);

                if (hitBool) {
                    if (!movedPassengers.Contains(hit.transform)) {
                        movedPassengers.Add(hit.transform);

                        float pushX = (directionY == 1) ? velocity.x : 0;
                        float pushY = velocity.y - (hit.distance - skinWidth) * directionY;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), directionY == 1, true));
                    }
                }
            }
        }

        // Horizontal platform
        if (velocity.x != 0) {
            float rayLenght = Mathf.Abs(velocity.x) + skinWidth;

            for (int i = 0; i < horizontalRayCount; i++) {
                Vector3 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += transform.up * (horizontalRaySpacing * i);
                RaycastHit hit;
                var hitBool = Physics.Raycast(rayOrigin, transform.right * directionX, out hit, rayLenght, passengerMask);

                Debug.DrawRay(rayOrigin, transform.right * directionX * rayLenght, Color.green);

                if (hitBool) {
                    if (!movedPassengers.Contains(hit.transform)) {
                        movedPassengers.Add(hit.transform);

                        float pushX = velocity.x - (hit.distance - skinWidth) * directionX;
                        float pushY = -skinWidth;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false, true));

                        //  hit.transform.Translate(transform.up * pushY);
                        //  hit.transform.RotateAround(targetCenter.position, Vector3.up, velocity.x * DistanceToAngles());
                    }
                }
            }
        }

        // Passenger on top of horizontal or downward
        if (directionY == -1 || velocity.y == 0 && velocity.x != 0) {
            float rayLenght = skinWidth * 2;

            for (int i = 0; i < verticalRayCount; i++) {
                Vector3 rayOrigin = raycastOrigins.topLeft + transform.right * (verticalRaySpacing * i);
                RaycastHit hit;
                var hitBool = Physics.Raycast(rayOrigin, transform.up, out hit, rayLenght, passengerMask);

                Debug.DrawRay(rayOrigin, transform.up * directionY * rayLenght, Color.red);

                if (hitBool) {
                    if (!movedPassengers.Contains(hit.transform)) {
                        movedPassengers.Add(hit.transform);

                        float pushX = velocity.x;
                        float pushY = velocity.y;

                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true, false));
                    }
                }
            }
        }
    }

    struct PassengerMovement {
        public Transform transform;
        public Vector3 velocity;
        public bool standingOnPlatform;
        public bool moveBeforePlatform;

        public PassengerMovement(Transform _transform, Vector3 _velocity, bool _standingOnPlatform, bool _moveBeforePlatform) {
            transform = _transform;
            velocity = _velocity;
            standingOnPlatform = _standingOnPlatform;
            moveBeforePlatform = _moveBeforePlatform;
        }
    }
}
