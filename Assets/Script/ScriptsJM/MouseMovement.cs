﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    Vector3 RotationCenter = new Vector3(0, 0, 0);
    public float MouseSpeed = 1; // Number of Blocks per transform.
    float MouseSpeedDegrees = 0;
    Vector3 MouseRotation;

    void Start()
    {
        MouseSpeedDegrees = MouseSpeed * 6;
        MouseRotation = new Vector3(0, MouseSpeedDegrees, 0);
    }
    void Update()
    {

        if (Input.GetKey(KeyCode.Q))
        {
            transform.RotateAround(RotationCenter, Vector3.up,  -MouseSpeedDegrees * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.RotateAround(RotationCenter, Vector3.up, MouseSpeedDegrees * Time.deltaTime);
        }
        
    }
}
