﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalBlocksBoom : MonoBehaviour
{
    public float blockHeight = 0f;
    public float downTime;
    public float upTime;
    float TimerCheck = 0f;
    public float travelSpeedDown = 8;
    public float travelSpeedUp = 2;
    bool goingUp = true;
    bool goingDown = false;
    float verticalPos = 0f;
    public bool startUp = true;




    public enum EStatePillar
    {
        eGoingDown,
        eGoingUp,
        eStayUp,
        eStayDown
    }
    EStatePillar state;
    void Start()
    {
        if (startUp == true)
        {
            state = EStatePillar.eGoingDown;
            verticalPos = blockHeight * 0.1f;
        }
        else
        {
            state = EStatePillar.eGoingUp;
            verticalPos = 0;
        }
    }
    void Update()
    {
        MoveBlocks();

    }

    void MoveBlocks()
    {
        switch (state)
        {
            case EStatePillar.eGoingUp:
                transform.Translate(Vector3.up * Time.deltaTime * travelSpeedUp * 0.1f * 1);
                verticalPos += travelSpeedUp * Time.deltaTime * 0.1f;
                if (verticalPos >= blockHeight * 0.1f) { TimerCheck = 0; state = EStatePillar.eStayUp; }
                break;
            case EStatePillar.eGoingDown:

                transform.Translate(Vector3.up * Time.deltaTime * travelSpeedDown * 0.1f * -1);
                verticalPos -= travelSpeedDown * Time.deltaTime * 0.1f;
                if (verticalPos <= 0) { TimerCheck = 0; state = EStatePillar.eStayDown; }
                break;
            case EStatePillar.eStayDown:

                TimerCheck += Time.deltaTime;
                if (TimerCheck >= downTime) { state = EStatePillar.eGoingUp; }
                break;
            case EStatePillar.eStayUp:
                TimerCheck += Time.deltaTime;
                if (TimerCheck >= upTime) { state = EStatePillar.eGoingDown; }
                break;
        }
    }
}
