﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (L_Controller3D))]

public class L_Player : MonoBehaviour
{
    public float maxJumpHeight = 2f;
    public float minJumpHeight = 1f;
    public float timeToJumpApex = 0.4f;
    public float accelerationTimeAirborne = 0.2f;
    public float accelerationTimeGrounded = 0.1f;
    public float moveSpeed = 2f;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    Vector3 StartPos;
    Quaternion StartRot;

    public float wallSlideSpeedMax = .3f;

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    L_Controller3D controller;

    [SerializeField] GameObject
        MouseModel;

    public bool
        OnWall,WallJump;

    void Start()
    {
        StorePosition();
        controller = GetComponent<L_Controller3D>();
        CalculatePhysics();

        OnWall = false;
    }

    void CalculatePhysics()
    {
        gravity = -(2 * (0.1f * maxJumpHeight)) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * (minJumpHeight * 0.1f));
        //print("Gravity =" + gravity + " Jump Velocity = " + jumpVelocity);
    }

    void Update() {

        var Horiz = Input.GetAxisRaw("Horizontal");
        var Verti = Input.GetAxisRaw("Vertical");
        Vector2 input = new Vector2(Horiz, Verti);
        //if (Horiz < 0)
        //{
        //    transform.eulerAngles = new Vector3(0, 180, 0);
        //}
        //else if (Horiz > 0)
        //{
        //    transform.eulerAngles = new Vector3(0, 0, 0);
        //}


        int wallDirX = (controller.collisions.left) ? -1 : 1;

        bool wallSliding = false;
        if((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax){
                velocity.y = -wallSlideSpeedMax;
            }

            if (WallJump)
            {
                WallJump = false;
            }
        }
        //controller3D.LookAtCenter();

        CalculatePhysics();

        if (controller.collisions.above || controller.collisions.below) {
            velocity.y = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow) || OVRInput.GetDown(OVRInput.Button.One)){
            if (wallSliding) {

                if (wallDirX == input.x) {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;
                }
                else if(input.x == 0) {
                    velocity.x = -wallDirX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;
                }
                else {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }

                MouseModel.GetComponent<AnimationCharacter>().TriggerWallJump();
                this.GetComponent<L_Controller3D>().IsGround = false;
                WallJump = true;
            }
            else if (!wallSliding)
            {
                if (controller.collisions.below)
                {
                    velocity.y = maxJumpVelocity;

                    MouseModel.GetComponent<AnimationCharacter>().TriggerJumping();
                    this.GetComponent<L_Controller3D>().IsGround = false;
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.UpArrow) || OVRInput.GetUp(OVRInput.Button.One)) {
            if (velocity.y > minJumpVelocity) {
                velocity.y = minJumpVelocity;
            }
        }

        if (Input.GetKey(KeyCode.R)) {
            ResetPosition();
        }

        if (Input.GetKey(KeyCode.T)) {
            StorePosition();
        }

        if (wallSliding)
        {
            if (!OnWall)
            {
                MouseModel.GetComponent<AnimationCharacter>().TriggerWallSlide();
                OnWall = true;
            }
        }
        else if (!wallSliding)
        {
            if (OnWall)
            {
                if (!WallJump)
                {
                    MouseModel.GetComponent<AnimationCharacter>().TriggerLanding();
                    OnWall = false;
                } 
            }
        }

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
    void StorePosition()
    {
        StartPos = transform.position;
        StartRot = transform.rotation;
    }
    void ResetPosition()
    {
        transform.position = StartPos;
        transform.rotation = StartRot;
        velocity.x = 0;
        velocity.y = 0;
    }
}