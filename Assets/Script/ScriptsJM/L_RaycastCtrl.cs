﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class L_RaycastCtrl : MonoBehaviour
{
    public LayerMask collisionMask;

    public const float skinWidth = .01f;
    Vector3 raycastInset;
    public int horizontalRayCount = 4;
    public int verticalRayCount = 4;

    public Transform targetCenter;

    [HideInInspector]
    public float horizontalRaySpacing;
    [HideInInspector]
    public float verticalRaySpacing;

    [HideInInspector]
    public BoxCollider collider;
    public RaycastOrigins raycastOrigins;

    public virtual void Start() {
        collider = GetComponent<BoxCollider>();
        CalculateRaySpacing();
    }

    public float DistanceToAngles() {
        float Radius = Vector3.Distance(Vector3.zero, new Vector3(transform.position.x, 0, transform.position.z));
        float Perimetre = 2 * Mathf.PI * Radius;
        float AnglePerUnit = 360 / Perimetre;
        return AnglePerUnit;
    }

    public void UpdateRaycastOrigins() {
        Vector3 colliderSize = new Vector3(collider.size.x, collider.size.y, collider.size.z);
        raycastInset = new Vector3(skinWidth / transform.localScale.x, skinWidth / transform.localScale.y, skinWidth / transform.localScale.z);

        Vector3 colliderSizeInset = new Vector3(colliderSize.x - raycastInset.x * 2, colliderSize.y - raycastInset.y * 2, colliderSize.z - raycastInset.z * 2);

        raycastOrigins.bottomLeft = transform.TransformPoint(collider.center + new Vector3(-colliderSizeInset.x / 2, -colliderSizeInset.y / 2, 0));
        raycastOrigins.bottomRight = transform.TransformPoint(collider.center + new Vector3(colliderSizeInset.x / 2, -colliderSizeInset.y / 2, 0));
        raycastOrigins.topLeft = transform.TransformPoint(collider.center + new Vector3(-colliderSizeInset.x / 2, colliderSizeInset.y / 2, 0));
        raycastOrigins.topRight = transform.TransformPoint(collider.center + new Vector3(colliderSizeInset.x / 2, colliderSizeInset.y / 2, 0));
    }

    public void CalculateRaySpacing() {

        horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
        verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

        horizontalRaySpacing = (collider.size.y * transform.localScale.y - 2 * skinWidth) / (horizontalRayCount - 1);
        verticalRaySpacing = (collider.size.x * transform.localScale.x - 2 * skinWidth) / (verticalRayCount - 1);
    }

    public struct RaycastOrigins {
        public Vector3 topLeft, topRight;
        public Vector3 bottomLeft, bottomRight;
    }
}
