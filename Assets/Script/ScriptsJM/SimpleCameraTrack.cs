﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCameraTrack : MonoBehaviour
{
    float PosY;
    public float SmoothingY = 1f;
    public float MaxSpeedY = .1f;
    float VelocityYSmoothing;

    Vector3 RotateY;
    public float RotationSmoothing = 6f;
    float VelocityRotateSmoothing;
    float RotateStep = 2f;
    Quaternion rotation;

    public Transform Player;

    void Start() {
        PosY = Player.transform.position.y;
    }

    void Update() {
        Height();
        RotationX();
        Move();
    }

    void Move() {
        transform.position = new Vector3(0, PosY, 0);
        transform.rotation = rotation;
    }

    void RotationX() {
        RotateY = new Vector3(Player.transform.position.x, 0, Player.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z);
        rotation = Quaternion.LookRotation(RotateY, Vector3.up);

    }

    void Height() {
        float TargetPosY;
        if (Player.transform.position.y >= .1f || Player.transform.position.y <= -.1f) {
            TargetPosY = Player.transform.position.y;
        }
        else {
            TargetPosY = PosY;
        }
        PosY = Mathf.SmoothDamp(PosY, TargetPosY, ref VelocityYSmoothing, SmoothingY, MaxSpeedY);
    }
}
