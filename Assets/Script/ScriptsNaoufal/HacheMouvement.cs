﻿using UnityEngine;
using System.Collections;

public class HacheMouvement : MonoBehaviour
{



    public Transform pivot;
    public float speed = 0.5f;
    private bool dragging = false;
    public float startAngle = 120f;
    public float endAngle = -120.0f;
    private float fTimer = 0.0f;
    private Vector3 v3T = Vector3.zero;
    float AchangerX;
    float AchangerY;


    void Start()
    {
        AchangerX = pivot.transform.eulerAngles.x;
        AchangerY = pivot.transform.eulerAngles.y;
    }

    void Update()
    {
        if (!dragging)
        {
            float f = (Mathf.Sin(fTimer * speed - Mathf.PI / 2.0f) + 1.0f) / 2.0f;

            v3T.Set(AchangerX, AchangerY, Mathf.Lerp(startAngle, endAngle, f));
            pivot.eulerAngles = new Vector3(AchangerX, AchangerY, v3T.z);
            fTimer += Time.deltaTime;
        }
    }
    
}
