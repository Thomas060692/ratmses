﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCharacter : MonoBehaviour
{
    Animator ThisAnimateur;
    public float speed = 0.1f;

    public string Land, Jump, Air, Fall, WallJump, WallSlide;

    // Start is called before the first frame update
    void Start()
    {
       ThisAnimateur = this.GetComponent<Animator>();
    }

    public void TriggerLanding()
    {
        ThisAnimateur.Play(Land);
    }

    public void TriggerJumping()
    {
        ThisAnimateur.Play(Jump);
    }

    public void TriggerInAir()
    {
        ThisAnimateur.Play(Air);
    }                  

    public void TriggerFalling()
    {
        ThisAnimateur.Play(Fall);
    }

    public void TriggerWallJump()
    {
        ThisAnimateur.Play(WallJump);
    }

    public void TriggerWallSlide()
    {
        ThisAnimateur.Play(WallSlide);
    }

    // Update is called once per frame
    void Update()
    {
        var Horiz = Input.GetAxisRaw("Horizontal");
        var Verti = Input.GetAxisRaw("Vertical");
        if (Horiz < 0)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
            //var slow = Quaternion.Euler(transform.rotation.x, 180, transform.rotation.z);

            //transform.rotation = Quaternion.Lerp(transform.rotation, slow, Time.deltaTime * speed);




        }
        else if (Horiz > 0)
        {


            //var slow = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);

            //transform.rotation = Quaternion.Lerp(transform.rotation, slow, Time.deltaTime * speed);
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }

        Animation();    
    }


    void Animation()
    {
        //Animation Idle/walk 
        float Horiz = Input.GetAxis("Horizontal");
        ThisAnimateur.SetFloat("InputX", Horiz);

        //Animation Jump
        bool space = Input.GetKeyDown(KeyCode.Space);

        /*
        if (space == true)
            ThisAnimateur.SetTrigger("InputSpace");
          */
    }

    private void OnTriggerEnter(Collider other)
    {
        string traps = other.gameObject.tag;

        switch (traps)
        {
            //case traps == "piegePick"
            //    {

            //    }

        }

    }
}
