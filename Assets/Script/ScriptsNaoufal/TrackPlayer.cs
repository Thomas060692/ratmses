﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayer : MonoBehaviour
{
    public GameObject characterHasFollow;
    public LayerMask groundDistance;
    public float lerpTolerance;
    public float speedLerp;

    [Header("Debug")]
    public float distanceOfGround;

    void Start()
    {
        transform.position = new Vector3(0, characterHasFollow.transform.position.y, 0);
    }

    void Update()
    {
        transform.LookAt(characterHasFollow.transform, Vector3.up);

        if (characterHasFollow != null)
        {
            RaycastHit hitGroundDistance;

            if (Physics.Raycast(characterHasFollow.transform.position, Vector3.down, out hitGroundDistance, Mathf.Infinity, groundDistance))
            {
                distanceOfGround = hitGroundDistance.distance;
            }

            var slow = new Vector3(transform.position.x, distanceOfGround, transform.position.z);

            if (characterHasFollow.transform.position.y - transform.position.y > lerpTolerance || characterHasFollow.transform.position.y - transform.position.y < -lerpTolerance)
            {
                transform.position = Vector3.Lerp(transform.position, slow, Time.deltaTime * speedLerp);
            }
        }
    }
}