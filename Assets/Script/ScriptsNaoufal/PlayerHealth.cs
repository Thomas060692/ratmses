﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 5;
    public int currentHealth;
    public int Damage = 1;
    public float timer = 10.0f;
    [Range(0.001f, 0.5f)]
    public float flickerSpeed = .15f;
    [Range(0, 10)]
    public int flickerNumber = 3;
    public string SceneName = "SceneFinale 1";
    public Vector3 Xposition;
    public Quaternion Xrotation;
    public Renderer Player;
    L_Player playerMovement;
    bool isDead;
    bool damaged;
    [Range(0, 10)]
    public float RspwnTime = 5;
    public Transform CheckPointTransform;
    public GameObject CheckPointSound;

    public event Action<Vector3> onPositionLastCheckPoint;

    void Awake()
    {
        //Player = GetComponent<Renderer>();
        playerMovement = GetComponent<L_Player>();
        currentHealth = startingHealth;
        Xposition = transform.position;
        Xrotation = transform.rotation;
        CheckPointTransform = null;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            StayInLife(Damage);

            timer = 10;
        }

    }



    public void StayInLife(int amount)
    {
        if (currentHealth < startingHealth && !isDead)
        {
            damaged = false;
            currentHealth += amount;
        }
    }

    bool flag = false;
    IEnumerator OnTriggerEnter(Collider other)
    {
        string tagTrap = other.gameObject.tag;

        if (!flag)
        {
            flag = true;

            if (currentHealth > 0 && tagTrap == "TrapBall" || tagTrap == "TrapPickWall" || tagTrap == "TrapPickGround" || tagTrap == "TrapBlock" || tagTrap == "TrapChopped")
            {
                TakeDamage(Damage);
                playerMovement.enabled = false;
                onPositionLastCheckPoint?.Invoke(Xposition);
                yield return new WaitForSeconds(RspwnTime);
                NewPosition();  //attendre des secondes pour le Death Animation.                
            }

            flag = false;
        }

        if (other.tag == "Checkpoint")
        {
            if (other.gameObject.transform.position != Xposition)
            {
                if (CheckPointTransform != null)
                {
                    StaticCheckpointEvent.CheckpointDesactivation(other.transform.position);
                    //CheckPointTransform.gameObject.GetComponentInChildren<CheckpointGlow>().IntensityReduce();
                }
                CheckPointTransform = other.transform;
                Xposition = CheckPointTransform.position;
                CheckPointPosition(other.transform.position);
            }
        }
    }

    void CheckPointPosition(Vector3 pos)
    {
        Debug.Log("Checkpoint pos stored");
        //Xposition = pos;
        Xrotation = transform.rotation;

    }


    void NewPosition()
    {

        Instantiate(CheckPointSound, transform.position, Quaternion.identity);


        GameObject[] killESoundCloune;
        killESoundCloune = GameObject.FindGameObjectsWithTag("CheckPointSound");
        for (int i = 0; i < killESoundCloune.Length; i++)
        {
            Destroy(killESoundCloune[i].gameObject, 2);
        }

        transform.position = Xposition;
        transform.rotation = Xrotation;

        playerMovement.enabled = true;

        StartCoroutine(Switch());
    }

    public IEnumerator Switch()
    {
        for (int i = 0; i < flickerNumber; i++)
        {
            Player.enabled = false;
            yield return new WaitForSeconds(flickerSpeed);

            Player.enabled = true;
            yield return new WaitForSeconds(flickerSpeed);
        }
        //playerMovement.enabled = true;
    }

    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;
        playerMovement.enabled = false;
        Invoke("LoadScene", 4);

    }

    void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }



}
