﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float walkSpeed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float runSpeed = 4.0F;
    public float gravity = 20.0F;
    Rigidbody rb;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;



    void Start()
    {
        controller = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //les Mouvements Horizontal/Vertical/Jump 
        if (controller.isGrounded )
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= walkSpeed;


            if (Input.GetKey(KeyCode.LeftAlt))
                moveDirection = new Vector3(moveDirection.x * runSpeed, moveDirection.y, moveDirection.z * runSpeed);


            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
                moveDirection = transform.TransformDirection(moveDirection);
            }

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}
