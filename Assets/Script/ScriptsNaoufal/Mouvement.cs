﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    [Range(10,100)]
    public int speed;
    [Range(10, 50)]
    public int Jumping;

    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.Space ))
            transform.position += new Vector3(Input.GetAxis("Horizontal") * speed *  Time.deltaTime, Jumping * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime);


    }
}