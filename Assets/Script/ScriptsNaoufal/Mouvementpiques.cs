﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvementpiques : MonoBehaviour
{
    Vector3 Pos;
    [Range(0, 0.1f)]
    public float HeightOut = 0.075f;
    [Range(0, 5)]
    public float Timer = 0.4f;
    float TimerCheck = 0;
    [Range(0, 20)]
    public float TimerUp = 5f;
    float TimerCheckUp = 0;
    [Range(0.2f, 2)]
    public float SpeedUp = 1f;
    [Range(0.1f, 1)]
    public float SpeedDown = .1f;
    bool Triggered = false;

    public GameObject PrefabPiqueSound;
    public GameObject PrefabPiquesSoundDown;

    void Start()
    {
        Pos = Vector3.zero;

    }

    void Update()
    {
        PicsMovement();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && Pos.y <= 0)
        {
            Triggered = true;
            Instantiate(PrefabPiqueSound, transform.position, Quaternion.identity);


            GameObject[] killEmAll;
            killEmAll = GameObject.FindGameObjectsWithTag("Bullet");
            for (int i = 0; i < killEmAll.Length; i++)
            {
                Destroy(killEmAll[i].gameObject, 2);
            }



        }
    }

    void PicsMovement()
    {
        if (Triggered)
        {
            TimerCheck += Time.deltaTime;
            if (Pos.y <= HeightOut && TimerCheck >= Timer)
            {

                transform.position += transform.forward * SpeedUp * Time.deltaTime;
                Pos.y += SpeedUp * Time.deltaTime;
                TimerCheckUp = 0f;
            }
            if (Pos.y >= HeightOut)
            {
                TimerCheck = 0;
                Triggered = false;
            }
        }

        if (Triggered == false && Pos.y > 0)
        {
            TimerCheckUp += Time.deltaTime;
            if (TimerCheckUp >= TimerUp)
            {
                transform.position += transform.forward * SpeedUp * Time.deltaTime * -1;
                Pos.y += SpeedUp * Time.deltaTime * -1;


                Instantiate(PrefabPiquesSoundDown, transform.position, Quaternion.identity);


                GameObject[] killEmAllDown;
                killEmAllDown = GameObject.FindGameObjectsWithTag("BulletDown");
                for (int i = 0; i < killEmAllDown.Length; i++)
                {
                    Destroy(killEmAllDown[i].gameObject, 2);
                }

            }
        }
    }
}
