﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public class Climb : MonoBehaviour
//{

//    bool _canClimb;
//    public Transform ladderTop;
//    public Rigidbody _myRigidBody;
//    public Collider collider;


//    // Start is called before the first frame update
//    void Start()
//    {
//        _myRigidBody = GetComponent<Rigidbody>();



//    }

//    // Update is called once per frame
//    void Update()
//    {

//        if (Input.GetKey(KeyCode.UpArrow) && _canClimb == true)
//        {
//            _myRigidBody.isKinematic = true;

//            transform.position = Vector3.Lerp(transform.position, ladderTop.transform.position, Time.deltaTime);
//        }

//    }



//    private void OnTriggerStay(Collider other)
//    {
//        if (collider.gameObject.tag == "Ladder")
//        {
//            _canClimb = true;
//            Debug.Log("Climb");






//        }

//    }




//}










[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class Climb : MonoBehaviour
{
    public float WalkSpeed = 5f;
    public float ClimbSpeed = 3f;
    public LayerMask wallMask;

    bool climbing;

    Vector3 wallPoint;
    Vector3 wallNormal;

    Rigidbody body;
    CapsuleCollider coll;

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();
        coll = GetComponent<CapsuleCollider>();
    }

    void FixedUpdate()
    {
        if (NearWall())
        {
            if (FacingWall())
            {
                // if player presses the climb button
                if (Input.GetKeyUp(KeyCode.C))
                {
                    climbing = !climbing;
                }
            }
            else
            {
                climbing = false;
            }
        }
        else
        {
            climbing = false;
        }

        if (climbing)
        {
            ClimbWall();
        }
        else
        {
            Walk();
        }
    }

    void Walk()
    {
        body.useGravity = true;

        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");
        var move = transform.forward * v + transform.right * h;

        ApplyMove(move, WalkSpeed);
    }

    bool NearWall()
    {
        return Physics.CheckSphere(transform.position, 3f, wallMask);
    }

    bool FacingWall()
    {
        RaycastHit hit;
        var facingWall = Physics.Raycast(transform.position, transform.forward, out hit, coll.radius + 1f, wallMask);
        wallPoint = hit.point;
        wallNormal = hit.normal;
        return facingWall;
    }

    void ClimbWall()
    {
        body.useGravity = false;

        GrabWall();

        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");
        var move = transform.up * v + transform.right * h;

        ApplyMove(move, ClimbSpeed);
    }

    void GrabWall()
    {
        var newPosition = wallPoint + wallNormal * (coll.radius - 0.1f);
        transform.position = Vector3.Lerp(transform.position, newPosition, 10 * Time.deltaTime);

        if (wallNormal == Vector3.zero)
            return;

        transform.rotation = Quaternion.LookRotation(-wallNormal);
    }

    void ApplyMove(Vector3 move, float speed)
    {
        body.MovePosition(transform.position + move * speed * Time.deltaTime);
    }
}

