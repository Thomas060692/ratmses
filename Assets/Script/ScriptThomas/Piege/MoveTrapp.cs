﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrapp : MonoBehaviour
{
    public PlayerTriggerTrap PlayerTriggerTrap;
    public float speedUpTrap;
    public float speedDownTrap;
    public float heightTrapp;
    public float delaiTriggTrap;
    public bool trapHeightState = false;
    public bool trapLowState = true;

    [Header("Debug")]
    public bool canContinueMovePick;
    public float heightTarppMax;
    public float LowTrapMin;

    Vector3 posTrap;

    float currentDelayTriggTrap;

    private void Start()
    {
        posTrap = transform.position;
        heightTarppMax = posTrap.y + heightTrapp;
        LowTrapMin = posTrap.y;
        currentDelayTriggTrap = delaiTriggTrap;
    }

    private void OnEnable()
    {
        PlayerTriggerTrap.OnTriggTrap += TriggTrap;
    }

    private void OnDisable()
    {
        PlayerTriggerTrap.OnTriggTrap -= TriggTrap;
    }

    private void Update()
    {
        if (canContinueMovePick == true)
        {
            currentDelayTriggTrap -= Time.deltaTime;
            //StartCoroutine(DelayTriggTrap());

            if (currentDelayTriggTrap < 0)
            {
                MovePick();
            }
        }
    }

    private void MovePick()
    {
        if (trapLowState == true)
        {
            if (posTrap.y < heightTarppMax)
            {
                posTrap = transform.position;
                transform.Translate(Vector3.up * speedUpTrap * Time.deltaTime);
            }
            else if (posTrap.y > heightTarppMax)
            {
                trapLowState = false;
                trapHeightState = true;
            }
        }

        if (trapHeightState == true)
        {
            if (posTrap.y > LowTrapMin)
            {
                posTrap = transform.position;
                transform.Translate(Vector3.down * speedDownTrap * Time.deltaTime);
            }
            else if (posTrap.y < LowTrapMin)
            {
                trapLowState = true;
                trapHeightState = false;
                canContinueMovePick = false;
            }
        }
    }

    private void TriggTrap(bool obj)
    {
        if (obj == true)
        {
            currentDelayTriggTrap = delaiTriggTrap;

            canContinueMovePick = true;
        }
    }

    //IEnumerator DelayTriggTrap()
    //{
    //    yield return new WaitForSeconds(delaiTriggTrap);
    //    MovePick();
    //}
}
