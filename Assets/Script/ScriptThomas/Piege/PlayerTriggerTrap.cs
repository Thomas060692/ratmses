﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTriggerTrap : MonoBehaviour
{
    public event Action<bool> OnTriggTrap;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnTriggTrap?.Invoke(true);
        }
        else
        {
            OnTriggTrap?.Invoke(false);
        }
    }
}
