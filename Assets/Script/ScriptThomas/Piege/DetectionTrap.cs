﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionTrap : MonoBehaviour
{
    [SerializeField] Animator deathAnimation;

    private void OnTriggerEnter(Collider other)
    {
        string trap = other.gameObject.tag;

        switch (trap)
        {
            case "TrapBall":
                deathAnimation.SetTrigger("TriggerDeathTrapBall");
                break;

            case "TrapPickWall":
                deathAnimation.SetTrigger("TriggerDeathTrapPickWall");
                break;

            case "TrapPickGround":
                deathAnimation.SetTrigger("TriggerDeathTrapPickGround");
                break;

            case "TrapBlock":
                deathAnimation.SetTrigger("TriggerDeathTrapBlock");
                break;

            case "TrapChopped":
                deathAnimation.SetTrigger("TriggerDeathTrapChopped");
                break;
        }
    }
}
