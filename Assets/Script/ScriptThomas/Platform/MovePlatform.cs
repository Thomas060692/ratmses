﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    public GameObject pivo;

    public float moveSpeed;
    public float timeToMovePlatform;
    public bool moveToLeft;
    public bool moveToRight;

    float currentTimeToMovePlatform;

    void Start()
    {
        moveToRight = true;
        moveToLeft = false;
    }

    void Update()
    {
        if (moveToRight == true)
        {
            if (currentTimeToMovePlatform < timeToMovePlatform)
            {
                transform.RotateAround(pivo.transform.position, Vector3.up, moveSpeed);
                currentTimeToMovePlatform += Time.fixedDeltaTime;                
            }
            else
            {
                moveToRight = false;
                moveToLeft = true;
            }
        }

        if (moveToLeft == true)
        {
            if (currentTimeToMovePlatform > 0)
            {
                transform.RotateAround(pivo.transform.position, Vector3.up, -moveSpeed);
                currentTimeToMovePlatform -= Time.fixedDeltaTime;
            }
            else
            {
                moveToRight = true;
                moveToLeft = false;
            }
        }
    }
}
