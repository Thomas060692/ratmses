﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPlayer : MonoBehaviour
{
    public LayerMask Ground;
    public Rigidbody rbPlayer;

    public float lenghtOfRaycast;
    public float velocity;
    public float timeJump;
    public bool canJump;
    public bool isGround;
    public bool isFallen;

    RaycastHit hitGround;

    float pointStartOfjump;
    float currentTimeJump;

    void Update()
    {
        currentTimeJump -= Time.deltaTime;

        if (Input.GetButtonDown("Jump"))
        {
            if (Physics.Raycast(transform.position, Vector3.down, out hitGround, lenghtOfRaycast, Ground))
            {
                canJump = true;
                currentTimeJump = timeJump;
            }
        }

        if (Input.GetButton("Jump") && canJump == true)
        {
            if (currentTimeJump > 0)
            {
                Jump();
                currentTimeJump -= Time.deltaTime;
            }
            else if (canJump == false)
            {
                //canJump = false;
                IsFallen();
            }
        }

        if(Input.GetButtonUp("Jump"))
        {
            IsFallen();
        }

        if (isGround == false && isFallen == true)
        {
            if (Physics.Raycast(transform.position, Vector3.down, out hitGround, lenghtOfRaycast, Ground))
            {
                isFallen = false;
                isGround = true;
            }
        }
    }

    void Jump()
    {
        isGround = false;
        canJump = false;
        pointStartOfjump = transform.position.y;
        rbPlayer.velocity = new Vector3(0.0f, velocity, 0.0f);
    }

    void IsFallen()
    {
        isFallen = true;
        isGround = false;
        rbPlayer.velocity = new Vector3(rbPlayer.velocity.x, 0.0f, rbPlayer.velocity.z);
    }
}
