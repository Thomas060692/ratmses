﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCharacter : MonoBehaviour
{
    [SerializeField] float jumpSpeed;    // la vitesse à la quelle le joueur saute
    [SerializeField] float fallenSpeed; // la vitesse de chute du joueur 
    [SerializeField] float hightOfJump;// la hauteur maximum du saute
    [SerializeField] float lenghtOfRaycast;
    [SerializeField] LayerMask GroundLayerMask; // le layer sur le quel la "physique" repose 

    public bool isGround = true; // detection vrai/faux du sol
    public bool fallen = false; // detection vrai/faux de la chute  
    public bool isJump = false;// detection vrai/faux du saute
    private float startJump;  // recuperation de la valeur du vecteur y a la position du joueur

    RaycastHit hit; // recuper les informatrions sur ce que le raycaste à touché

    void FixedUpdate()
    {
        Debug.DrawRay(transform.position, new Vector3(0f, -lenghtOfRaycast, 0f), Color.black);

        if (Input.GetButtonDown("Jump") && isGround) // la touche saut à été enfoncer
        {
            isGround = false;                   // le joueur ne touche plus le sol
            isJump = true;                     // le joueur est en train de sauter ou de chuter
            startJump = transform.position.y; // prise de la vauleur de hauteur pour calculer la hauteur de saut
        }

        FallFromPlatform();

        if (fallen == false) // si le joueur ne tombe pas il peut sauter
        {
            Jump(); // appelle de la méthode pour faire sauter le joueur
        }
        else // si non il tombe
        {
            Fallen();
        }
    }

    public void FallFromPlatform()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out hit, lenghtOfRaycast, GroundLayerMask) == false && isJump == false) // détection de la chute du joueur 
        {
            isGround = false; // dans ce cas le joueur ne touche plus de sol
            fallen = true;   // dit que le joueur tombe
            Fallen();       // appelle de la méthode fallen pour faire tomber le joueur
        }
    }

    private void Fallen() // méthode pour fair tomber le joueur
    {
        if (fallen == true) // si il tombe 
        {
            transform.Translate(Vector3.down * fallenSpeed * Time.deltaTime); // fonction qui fais tombé le joueur 

            if (Physics.Raycast(transform.position, Vector3.down, out hit, lenghtOfRaycast, GroundLayerMask)) // si le joueur touche un sol
            {
                fallen = false;   // arrête la chute
                isGround = true; // sol toucher
                isJump = false; // n'est plus en saut
            }
        }
    }

    private bool Jump() // méthode saut
    {
        if (isGround == false) // si le sol n'est plus detecter
        {
            transform.Translate(Vector3.up * jumpSpeed * Time.deltaTime); // fontion qui fais monté le joueur

            if (transform.position.y - startJump >= hightOfJump) // calcule de la hauteur du saut pour enclancher la chute
            {
                return fallen = true; // retourne vrais pour la chute si la hauteur à été atteinte
            }
        }
        return fallen = false; // retourne faux pour rien
    }
}
