﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float moveSpeed;
    public float slowFallen;
    
    public JumpFinal JumpFinal;

    void Update()
    {
        float currentSpeed = JumpFinal.isFallen ? (currentSpeed = moveSpeed / slowFallen) : currentSpeed = moveSpeed;

        float moveForward = Input.GetAxis("Vertical") * currentSpeed * Time.deltaTime;
        float moveRight = Input.GetAxis("Horizontal") * currentSpeed * Time.deltaTime;

        transform.Translate(moveRight, 0.0f, moveForward);
    }
}
