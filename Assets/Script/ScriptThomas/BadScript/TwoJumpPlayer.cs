﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoJumpPlayer : MonoBehaviour
{
    public Rigidbody rbPlayer;
    public LayerMask ground;
    public float timeButtonOfJumpDown;
    public float velocity;
    public bool canJump;
    public bool isFallen;

    private float currentTimeButtonOfJumpDown;
    private RaycastHit hitGround;

    void Update()
    {
        currentTimeButtonOfJumpDown += Time.deltaTime;

        if (Physics.Raycast(transform.position, Vector3.down, out hitGround, 0.4f, ground))
        {
            canJump = true;
            isFallen = false;
        }

        if (Input.GetButtonDown("Jump"))
        {
            currentTimeButtonOfJumpDown = 0;
            canJump = false;
        }

        if (Input.GetButton("Jump") && currentTimeButtonOfJumpDown <= timeButtonOfJumpDown && canJump == true)
        {

            if (currentTimeButtonOfJumpDown < timeButtonOfJumpDown / 2)
            {
                LowJump();
            }
            else if (currentTimeButtonOfJumpDown > timeButtonOfJumpDown / 2)
            {
                HeightJump();
            }
        }
    }

    private void LowJump()
    {
        rbPlayer.velocity = new Vector3(0.0f, velocity / 2, 0.0f);
    }

    private void HeightJump()
    {
        rbPlayer.velocity = new Vector3(0.0f, velocity, 0.0f);
        isFallen = true;
    }
}
