﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpirit : MonoBehaviour
{
    [SerializeField] float speedSpirit;
    [SerializeField] Animation floatingSpiritAnimation;
    [SerializeField] float delayToMovePirit;
    Vector3 positionCheckPoint;
    GameObject player;

    public void Initialize(Vector3 pos, GameObject _player)
    {
        positionCheckPoint = pos;
        player = _player;
    }

    void Update()
    {
        StartCoroutine(DelayMoveSpirit());
    }

    IEnumerator DelayMoveSpirit()
    {
        yield return new WaitForSeconds(delayToMovePirit);
        floatingSpiritAnimation.Play(floatingSpiritAnimation.clip.name = "floatingSpirit");
        transform.LookAt(positionCheckPoint);
        transform.Translate(Vector3.forward * speedSpirit * Time.deltaTime);
    }
}
