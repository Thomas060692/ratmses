﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySpirit : MonoBehaviour
{
    [SerializeField] float timeBeforeDestroy;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Spirit")
        {
            Destroy(other.gameObject, timeBeforeDestroy);
        }
    }
}
