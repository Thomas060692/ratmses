﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevierScript : MonoBehaviour
{
    [SerializeField] GameObject pivoLevier;
    [SerializeField] float speed;
    [SerializeField] float angleActiveLevier;

    bool openDoor = false;

    Vector3 currentRotation;

    Vector3 rotation;

    public EventHandler<OpenDoorEventArgs> onOpenTheDoorEventHandler;

    void Start()
    {
        rotation = currentRotation = transform.localEulerAngles;
    }

    public void LevierAction()
    {
        if (rotation.x < currentRotation.x + angleActiveLevier)
        {
            rotation.x += speed * Time.deltaTime;
            transform.localEulerAngles = rotation;
        }
        else
        {
            if (openDoor == false)
            {
                openDoor = true;
                OnOpenTheDoorEventHandler();
            }
        }
    }

    public void LevierRest()
    {
        if (rotation.x > currentRotation.x)
        {
            rotation.x -= speed * Time.deltaTime;
            transform.localEulerAngles = rotation;
        }
        else
        {
            openDoor = false;
        }
    }

    public void OnOpenTheDoorEventHandler()
    {
        onOpenTheDoorEventHandler?.Invoke(this, new OpenDoorEventArgs { OpenDoor = openDoor });
    }
}
