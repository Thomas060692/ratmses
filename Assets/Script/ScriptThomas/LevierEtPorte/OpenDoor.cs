﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] LevierScript levier;
    [SerializeField] float hightOpenDoor;
    [SerializeField] float speed;
    [SerializeField] float timeToCloseDoor;
    [SerializeField] bool doorCanClosable;

    Vector3 currentPositionDoor;

    float doorOpening;
    float currentTimeToCloseDoor;
    bool doorGoClose = false;
    bool doorIsClose = false;
    bool doorGoOpen = false;
    bool doorIsOpen = false;

    void Start()
    {
        currentPositionDoor = transform.position;
        doorOpening = currentPositionDoor.y + hightOpenDoor;
    }

    private void OnEnable()
    {
        levier.onOpenTheDoorEventHandler += OpenDoorAction;
    }

    private void OnDisable()
    {
        levier.onOpenTheDoorEventHandler -= OpenDoorAction;
    }

    void Update()
    {
        openDoorAvction();

        if (doorCanClosable == true)
        {
            CloseDoorAction();

            if (doorGoClose == true || doorIsClose == true)
            {
                levier.LevierRest();

                if (doorIsClose == true)
                {
                    doorGoClose = false;
                    doorIsClose = false;
                    doorIsOpen = false;
                }
            }
        }
    }

    private void CloseDoorAction()
    {
        if (currentTimeToCloseDoor > 0)
        {
            currentTimeToCloseDoor -= Time.deltaTime;
            doorGoClose = true;
        }

        if (doorGoClose == true && doorIsClose == false && currentTimeToCloseDoor < 0)
        {
            if (currentPositionDoor.y > doorOpening - hightOpenDoor)
            {
                transform.Translate(Vector3.down * speed * Time.deltaTime);
                currentPositionDoor.y = transform.position.y;
            }
            else
            {
                doorGoClose = false;
                doorIsClose = true;
            }
        }
    }

    void openDoorAvction()
    {
        if (doorGoOpen == true && doorIsOpen == false)
        {
            if (currentPositionDoor.y < doorOpening)
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
                currentPositionDoor.y = transform.position.y;
            }
            else
            {
                doorIsOpen = true;
                doorGoOpen = false;
                currentTimeToCloseDoor = timeToCloseDoor;
            }
        }
    }

    private void OpenDoorAction(object sender, OpenDoorEventArgs e)
    {
        doorGoOpen = e.OpenDoor;
    }
}
