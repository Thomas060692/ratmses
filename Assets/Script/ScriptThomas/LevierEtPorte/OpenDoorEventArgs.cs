﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorEventArgs : EventArgs
{
    public bool OpenDoor { get; set; }
}
