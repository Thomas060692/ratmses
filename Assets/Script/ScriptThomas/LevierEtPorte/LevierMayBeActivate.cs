﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevierMayBeActivate : MonoBehaviour
{
    [SerializeField] LevierScript LevierScript;

    bool mayBeActivate;
    bool triggButtonAction;

    void Update()
    {
        if (mayBeActivate)
        {
            triggButtonAction = Input.GetButton("Fire1");

            if (triggButtonAction == true)
            {
                LevierScript.LevierAction();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            mayBeActivate = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            mayBeActivate = false;
        }
    }
}
