﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] float delayLoadScene;
    [SerializeField] string nameGameScene;
    [SerializeField] Animator fadeOut;
    [SerializeField] GameObject image;

    public void StartAdventure()
    {
        Time.timeScale = 1;

        image.SetActive(true);
        fadeOut.SetTrigger("FadeOut");
        StartCoroutine(DelayFadeOut());
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator DelayFadeOut()
    {
        yield return new WaitForSeconds(delayLoadScene);
        SceneManager.LoadScene(nameGameScene);
    }
}
