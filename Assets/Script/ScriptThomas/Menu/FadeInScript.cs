﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInScript : MonoBehaviour
{
    [SerializeField] float timeFadeIn;
    [SerializeField] Animator fadeIn;

    private void Start()
    {
        FadeIn();
    }

    void FadeIn()
    {
        fadeIn.SetTrigger("FadeIn");
        StartCoroutine(FadeInDisable());
    }

    IEnumerator FadeInDisable()
    {
        yield return new WaitForSeconds(timeFadeIn);
        transform.gameObject.SetActive(false);
    }
}
