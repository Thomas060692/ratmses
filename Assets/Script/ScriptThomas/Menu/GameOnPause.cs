﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOnPause : MonoBehaviour
{
    [SerializeField] Canvas gameCanvas;

    void Start()
    {
        gameCanvas.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            Time.timeScale = 0;
            gameCanvas.gameObject.SetActive(true);
        }
    }
}
