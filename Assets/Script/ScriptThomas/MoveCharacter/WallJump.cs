﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : MonoBehaviour
{
    [SerializeField] LayerMask wallLayer;
    [SerializeField] Transform pivoTransform;
    [SerializeField] float lenghtOfRaycast;
    [SerializeField] float delayBeforWalljump;
    [SerializeField] float forceToWallJump;
    [SerializeField] float timeToSnapWall;
    [SerializeField] bool canWallJump;
    [SerializeField] bool wallDetected;
    [SerializeField] bool isOnWall;

    float currentTimeSanpWall;
    bool buttonJumpStat;

    [Header("Debug")]
    public bool wallRightHit;
    public bool wallLeftHit;

    void FixedUpdate()
    {
        wallDetected = DetectionOfWall(ref wallRightHit, ref wallLeftHit);
        buttonJumpStat = ButtonJumpTriggForWallJump();

        if (wallDetected && canWallJump)
        {
            if (wallDetected)
            {
                isOnWall = true;

                if (isOnWall)
                {
                    currentTimeSanpWall -= Time.deltaTime;
                    if (currentTimeSanpWall > 0)
                    {
                        gameObject.GetComponent<Rigidbody>().useGravity = false;
                        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                    }
                    else
                    {
                        gameObject.GetComponent<Rigidbody>().useGravity = true;
                    }
                }
            }
            else
            {
                isOnWall = false;
            }

            if (isOnWall && buttonJumpStat)
            {
                StartCoroutine(DelayBeforWalljump());
            }
        }
        else
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void WallJumping()
    {
        if (wallRightHit)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(0f, forceToWallJump, 0f, ForceMode.Impulse);
            transform.RotateAround(pivoTransform.position, Vector3.up, -1.0f);
        }

        if (wallLeftHit)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(0f, forceToWallJump, 0f, ForceMode.Impulse);
            transform.RotateAround(pivoTransform.position, Vector3.up, 1.0f);
        }
    }

    bool ButtonJumpTriggForWallJump()
    {
        bool buttonJumpTrigg = false;

        if (Input.GetButtonDown("Jump"))
        {
            buttonJumpTrigg = true;
            currentTimeSanpWall = timeToSnapWall;
        }

        return buttonJumpTrigg;
    }

    bool DetectionOfWall(ref bool wR, ref bool wL)
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), lenghtOfRaycast, wallLayer))
        {
            if (!wR)
            {
                wR = true;
                wL = false;
                canWallJump = true;
                if (currentTimeSanpWall <= 0)
                {
                    currentTimeSanpWall = timeToSnapWall;
                }
            }
        }
        else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), lenghtOfRaycast, wallLayer))
        {
            if (wR)
            {
                wR = false;
                canWallJump = false;
                isOnWall = false;
            }
        }

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), lenghtOfRaycast, wallLayer))
        {
            if (!wL)
            {
                wR = false;
                wL = true;
                canWallJump = true;
                if (currentTimeSanpWall <= 0)
                {
                    currentTimeSanpWall = timeToSnapWall;
                }
            }
        }
        else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), lenghtOfRaycast, wallLayer))
        {
            if (wL)
            {
                wL = false;
                canWallJump = false;
                isOnWall = false;
            }
        }

        return wR | wL;
    }

    IEnumerator DelayBeforWalljump()
    {
        yield return new WaitForSeconds(delayBeforWalljump);
        WallJumping();
    }
}
