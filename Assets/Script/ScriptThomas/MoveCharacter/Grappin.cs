﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    [SerializeField] LayerMask grabLayer;
    [SerializeField] Transform pivo;
    [SerializeField] float lenghtOfRaycast;
    [SerializeField] float speedGrab;
    [SerializeField] float force;
    [SerializeField] bool isGrab;

    [Header("Debug")]
    public GameObject objectGrabbable;
    public float f_direction;

    RaycastHit grabHit;
    bool buttonGrabTrigg;

    void Update()
    {
        float gravity = Physics.gravity.y;
        float root = Mathf.Sqrt(2 / Mathf.Abs(gravity));
        float pi = Mathf.PI;
        float pendulum = 2 * pi * root;
        Debug.Log(-pendulum * Time.deltaTime * 100);

        buttonGrabTrigg = ButtonGrabTrigg();

        if (buttonGrabTrigg)
        {
            ObjectGrabbable();

            if (isGrab)
            {
                GrabMove();
            }
        }
        else if (buttonGrabTrigg && objectGrabbable == null)
        {
            Rest();
        }
    }

    private void LateUpdate()
    {
        if (isGrab && buttonGrabTrigg)
        {
            transform.RotateAround(pivo.transform.position, Vector3.up, speedGrab);
        }
    }

    private void GrabMove()
    {
        float trueDirection = Mathf.Sign(f_direction);
        Debug.Log("trueDirection : " + trueDirection);

        if (trueDirection > 0)
        {
            Vector3 direction = (transform.position - objectGrabbable.transform.position).normalized;
            Vector3 fixeDirectionX = new Vector3(direction.x, 0, 0);

            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(fixeDirectionX.x, f_direction * force, 0), ForceMode.Force);
        }
        if (trueDirection < 0)
        {
            Vector3 direction = (transform.position - objectGrabbable.transform.position).normalized;
            Vector3 fixeDirectionX = new Vector3(direction.x, 0, 0);

            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(fixeDirectionX.x, f_direction * force, 0), ForceMode.Force);
        }
    }

    private bool ButtonGrabTrigg()
    {
        bool buttonTrigg = false;

        if (Input.GetButtonDown("Fire1"))
        {
            buttonTrigg = true;
        }

        if (Input.GetButton("Fire1") && objectGrabbable == null)
        {
            buttonTrigg = true;
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<WallJump>().enabled = false;
            gameObject.GetComponent<WallClimable>().enabled = false;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            buttonTrigg = false;
            isGrab = false;
            f_direction = 0;
            Rest();
        }

        return buttonTrigg;
    }

    private void ObjectGrabbable()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out grabHit, lenghtOfRaycast, grabLayer))
        {
            isGrab = true;
            objectGrabbable = grabHit.collider.gameObject;
            f_direction = Vector3.Distance(transform.position, objectGrabbable.transform.position);
        }

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out grabHit, lenghtOfRaycast, grabLayer))
        {
            isGrab = true;
            objectGrabbable = grabHit.collider.gameObject;
            f_direction = -Vector3.Distance(transform.position, objectGrabbable.transform.position);
        }
    }

    private void Rest()
    {
        isGrab = false;
        objectGrabbable = null;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
        gameObject.GetComponent<WallJump>().enabled = true;
        gameObject.GetComponent<WallClimable>().enabled = true;
    }
}
