﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpFinal : MonoBehaviour
{
    public LayerMask groundLayer;
    public float lenghtOfRaycast;
    public float jumpForce;
    public float limitOfJump;
    public float forceForFloat;
    public float timeOfFloat;
    public float timeOfJump;
    public bool canJump;
    public bool isJump;
    public bool isFloat;
    public bool isFallen;

    float restTimeMutiplicator = 1;
    float currentTimeOfJump;
    float currentTimeMultiplicator;
    float currentTimeOfFloat;

    void Update()
    {
        if (Physics.Raycast(transform.position, Vector3.down, lenghtOfRaycast, groundLayer))
        {
            canJump = true;
            isJump = false;
            isFloat = false;
            isFallen = false;
            currentTimeOfJump = timeOfJump;
        }

        if (canJump == true || isJump == true)
        {
            ButtonJumpTrigg();
        }

        if (isFloat == true)
        {
            FloatJump();
        }
    }

    void FloatJump()
    {
        if (currentTimeOfFloat > 0)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, forceForFloat * Time.deltaTime, 0.0f));
            currentTimeOfFloat -= Time.deltaTime;
            isFallen = true;
        }
        else
        {
            isFloat = false;
        }
    }

    public void Jump()
    {
        isJump = true;
        float multi = Mutiplicateur();

        if (currentTimeMultiplicator >= limitOfJump)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.zero);
            isFloat = true;
        }
        else
        {
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, jumpForce, 0.0f), ForceMode.Impulse);
        }
    }

    void ButtonJumpTrigg()
    {
        if (Input.GetButtonDown("Jump"))
        {
            currentTimeMultiplicator = restTimeMutiplicator;
            isJump = true;
        }

        if (Input.GetButton("Jump") && currentTimeOfJump > 0)
        {
            canJump = false;
            currentTimeOfFloat = timeOfFloat;
            currentTimeOfJump -= Time.deltaTime;

            Jump();
        }

        if (Input.GetButtonUp("Jump"))
        {
            isFloat = true;
            isJump = false;
        }
    }

    float Mutiplicateur()
    {
        float multiplucateur = 1f;

        currentTimeMultiplicator += Time.fixedDeltaTime;
        if (currentTimeMultiplicator > limitOfJump)
        {
            return multiplucateur += 0.01f;
        }

        return multiplucateur;
    }
}
