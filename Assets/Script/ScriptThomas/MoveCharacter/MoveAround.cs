﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAround : MonoBehaviour
{
    public LayerMask Ground;
    public GameObject pivotObject;
    public JumpFinal JumpFinal;
    public float lenghtOfRaycast;

    public float moveSpeed;
    public float slowFallen;

    RaycastHit HitFrontDround;
    float currentSpeed;
    float direction;

    void Update()
    {
        currentSpeed = JumpFinal.isFallen ? (currentSpeed = moveSpeed / slowFallen) : currentSpeed = moveSpeed;
        direction = Input.GetAxisRaw("Horizontal") * currentSpeed * Time.deltaTime;

        if (direction < 0 && Physics.Raycast(transform.position + Vector3.down * 0.035f, transform.TransformDirection(Vector3.left), out HitFrontDround, lenghtOfRaycast, Ground))
        {
            direction = 0;
        }
        if (direction > 0 && Physics.Raycast(transform.position + Vector3.down * 0.035f, transform.TransformDirection(Vector3.right), out HitFrontDround, lenghtOfRaycast, Ground))
        {
            direction = 0;
        }

        transform.RotateAround(pivotObject.transform.position, Vector3.up, direction);

        //if (direction < 0)
        //{
        //  placer l'animation de marche à gauche
        //}
        //else if (direction > 0)
        //{
        //  placer l'animation de marche à droite
        //}
    }
}
