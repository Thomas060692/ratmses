﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallClimable : MonoBehaviour
{
    public LayerMask wallClimbableLayer;
    public float lenghtOfRaycast;
    public bool wallClimbableTouche;

    RaycastHit wallClimbalbeHit;

    void Update()
    {
        wallClimbableTouche = DetectionWallClimbable();

        if (wallClimbableTouche == true)
        {
            gameObject.GetComponent<WallJump>().enabled = false;
            wallClimb();
        }
        else
        {
            gameObject.GetComponent<WallJump>().enabled = true;
            gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void wallClimb()
    {
        float climbSpeed = ClimbSpeed();

        if (climbSpeed > 0 || climbSpeed < 0)
        {
            transform.Translate(Vector3.up * climbSpeed * 0.5f * Time.deltaTime);
            gameObject.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    float ClimbSpeed()
    {
        float wallClimabableSpeed = Input.GetAxis("Vertical");
        return wallClimabableSpeed;
    }

    bool DetectionWallClimbable()
    {
        bool wallClimbableDetected = false;

        if (Physics.Raycast(transform.position + Vector3.down * 0.035f, transform.TransformDirection(Vector3.right), out wallClimbalbeHit, lenghtOfRaycast, wallClimbableLayer))
        {
            wallClimbableDetected = true;
        }

        if (Physics.Raycast(transform.position + Vector3.down * 0.035f, transform.TransformDirection(Vector3.left), out wallClimbalbeHit, lenghtOfRaycast, wallClimbableLayer))
        {
            wallClimbableDetected = true;
        }

        return wallClimbableDetected;
    }
}
