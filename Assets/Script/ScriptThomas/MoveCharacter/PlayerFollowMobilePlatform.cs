﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowMobilePlatform : MonoBehaviour
{
    public LayerMask platformLayer;
    public float lenghtOfRaycast;

    RaycastHit platformHit;

    void Update()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out platformHit, lenghtOfRaycast, platformLayer))
        {
            transform.SetParent(platformHit.collider.gameObject.transform, true);
        }
        else if (!Physics.Raycast(transform.position, Vector3.down, out platformHit, lenghtOfRaycast, platformLayer))
        {
            transform.parent = null;
        }
    }
}
