﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationGrabbableObject : MonoBehaviour
{
    void Awake()
    {
        transform.LookAt(new Vector3(0, transform.position.y, 0));
    }
}
