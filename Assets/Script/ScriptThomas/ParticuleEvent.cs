﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticuleEvent : MonoBehaviour
{
    [SerializeField] GameObject footsteps;
    [SerializeField] GameObject squeak;
    [SerializeField] GameObject jumpPush;
    [SerializeField] GameObject jumpLand;
    [SerializeField] GameObject wallJump;
    [SerializeField] GameObject wallSlide;
    [SerializeField] AudioClip[] squeakingclips;
    [SerializeField] AudioClip[] footstepsclips;
    [SerializeField] AudioClip soundDeath;
    [SerializeField] AudioClip soundJumpPush;
    [SerializeField] AudioClip soundJumpLand;
    [SerializeField] AudioClip soundWallJump;
    [SerializeField] AudioClip soundWallSlide;

    [SerializeField] AudioSource audioSourceSqueaking;
    [SerializeField] AudioSource audioSourceFootsteps;
    [SerializeField] AudioSource audioSourceJumpLand;
    [SerializeField] AudioSource audioSourceJumpPush;
    [SerializeField] AudioSource audioSourceWallJump;
    [SerializeField] AudioSource audioSourceWallSlide;
    [SerializeField] AudioSource audioSourceSqueak;
    [SerializeField] AudioSource audioSourceDeath;

    
    private void Awake ()
    {
        audioSourceFootsteps = GetComponent<AudioSource>();
        audioSourceJumpLand = GetComponent<AudioSource>();
        audioSourceJumpPush = GetComponent<AudioSource>();
        audioSourceWallJump = GetComponent<AudioSource>();
        audioSourceWallSlide = GetComponent<AudioSource>();
        audioSourceSqueaking = GetComponent<AudioSource>();
        audioSourceDeath = GetComponent<AudioSource>();
    }

    public void Footsteps()
    {
        Instantiate(footsteps, transform.position, Quaternion.identity);
        AudioClip clip = GetRandomClipFootsteps();
        audioSourceFootsteps.PlayOneShot(clip);
    }

    private AudioClip GetRandomClipFootsteps()
    {
        return footstepsclips[UnityEngine.Random.Range(0, footstepsclips.Length)];
    }

    public void Squeak()
    {
        //Instantiate(squeak, transform.position, Quaternion.identity);
        AudioClip clip = GetRandomClipSqueaking();
        audioSourceSqueaking.PlayOneShot(clip);
    }

    private AudioClip GetRandomClipSqueaking()
    {
        return squeakingclips[UnityEngine.Random.Range(0, squeakingclips.Length)];
    }

    public void JumpPush()
    {
        Instantiate(jumpLand, transform.position, Quaternion.identity);
        audioSourceJumpPush.PlayOneShot(soundJumpPush);
    }

    public void JumpLand()
    {
        Instantiate(jumpLand, transform.position, Quaternion.identity);
        audioSourceJumpLand.PlayOneShot(soundJumpLand);
    }

    public void WallJump()
    {
        Instantiate(wallJump, transform.position, Quaternion.identity);
        audioSourceWallJump.PlayOneShot(soundWallJump);
    }

    public void WallSlide()
    {
        Instantiate(footsteps, transform.position, Quaternion.identity);
        audioSourceWallSlide.PlayOneShot(soundWallSlide);
    }

    //public void Squeak()
    //{
    //    audioSourceSqueak.PlayOneShot(soundSqueak);
        //if (L_Player.Horiz > 0)
        //    {
        //    if(!soundSqueak.isPlaying)
                
        //    soundSqueak.Pause();
        //    }
        
    //}

    public void Death()
    {
        Instantiate(footsteps, transform.position, Quaternion.identity);
        audioSourceDeath.PlayOneShot(soundDeath);
    }
}


//public class ParticuleEvent : MonoBehaviour
//{
//    [SerializeField] GameObject particuleWalk;
//    [SerializeField] GameObject particuleJump;

//    public void InstantiateParticule()
//    {
//        Instantiate(particuleWalk, transform.position, Quaternion.identity);
//    }

//    public void InstantiateParticuleJump()
//    {
//        Instantiate(particuleJump, transform.position, Quaternion.identity);
//    }
//}
