﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToCheckPoint : MonoBehaviour
{
    [SerializeField] GameObject spiritPrefab;
    [SerializeField] PlayerHealth playerHealth;
    [SerializeField] float heightOfSpawnspirit;
    [SerializeField] float speedSpirit;
    [Range(0,3)]
    [SerializeField] float delay;

    Vector3 positionCheckPoint;
    GameObject spirit;

    private void OnEnable()
    {
        playerHealth.onPositionLastCheckPoint += PositionOfCheckPoint;
    }

    private void OnDisable()
    {
        playerHealth.onPositionLastCheckPoint -= PositionOfCheckPoint;
    }

    private void PositionOfCheckPoint(Vector3 arg1)
    {
        positionCheckPoint = arg1;

        StartCoroutine(DelaySpawnSpirit());
    }

    IEnumerator DelaySpawnSpirit()
    {
        yield return new WaitForSeconds(delay);
        spirit = Instantiate(spiritPrefab, transform.position + new Vector3(0, heightOfSpawnspirit, 0), Quaternion.identity);
        spirit.GetComponent<MoveSpirit>().Initialize(positionCheckPoint, gameObject);
    }
}
