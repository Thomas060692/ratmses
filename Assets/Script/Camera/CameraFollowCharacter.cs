﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowCharacter : MonoBehaviour
{
    public GameObject characterHasFollow;
    public LayerMask groundDistance;

    float distanceOfGround;

    void Update()
    {
        transform.LookAt(characterHasFollow.transform.position, Vector3.up);

        RaycastHit hitGroundDistance;

        if (Physics.Raycast(characterHasFollow.transform.position, Vector3.down, out hitGroundDistance, Mathf.Infinity, groundDistance))
        {
            distanceOfGround = hitGroundDistance.distance;
        }

        transform.position = new Vector3(transform.position.x, distanceOfGround, transform.position.z);
    }
}
