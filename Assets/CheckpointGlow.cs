﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointGlow : MonoBehaviour
{
    [SerializeField] Animator glowAnimationCheckPoint;

    private void Start()
    {
        StaticCheckpointEvent.OnCheckpointChange += StaticCheckpointEvent_OnCheckpointChange;
    }

    private void OnDestroy()
    {
        StaticCheckpointEvent.OnCheckpointChange -= StaticCheckpointEvent_OnCheckpointChange;
    }

    private void StaticCheckpointEvent_OnCheckpointChange(Vector3 obj)
    {
        if(this.gameObject.transform.position!=obj)
        {
            glowAnimationCheckPoint.SetBool("CheckpointActivation", false);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            glowAnimationCheckPoint.SetBool("CheckpointActivation", true);
        }
    }

    //[SerializeField]
    //Renderer localRenderer;
    //[SerializeField]
    //float timer;
    //[SerializeField]
    //float currentIntensity = 0;
    //[SerializeField]
    //float maxIntensity;
    //[SerializeField]
    //float intensitySwitchSpeed;
    //public float Timer { get => timer; set => timer = value; }
    //public float CurrentIntensity { get => currentIntensity; set => currentIntensity = value; }
    //public float MaxIntensity { get => maxIntensity; set => maxIntensity = value; }
    //public Renderer LocalRenderer { get => localRenderer; set => localRenderer = value; }
    //public float IntensitySwitchSpeed { get => intensitySwitchSpeed; set => intensitySwitchSpeed = value; }
    //bool stop;
    //private void Start()
    //{
    //    stop = false;
    //    CurrentIntensity = CurrentIntensity;
    //    LocalRenderer.material.SetColor("_EmissionColor", Color.white * CurrentIntensity);
    //}
    //public void IntensityChange()
    //{

    //    while (CurrentIntensity < MaxIntensity)
    //    {
    //        StartCoroutine(TimerDelay());
    //    }
    //}
    //public void IntensityReduce()
    //{

    //    while (CurrentIntensity >0)
    //    {

    //        StartCoroutine(TimerDelayEnd());
    //    }
    //}
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (!stop)
    //    {
    //        if (other.gameObject.tag == "Player")
    //        {
    //            stop = true;
    //            IntensityChange();
    //        }
    //    }
    //}
    //IEnumerator TimerDelay()
    //{
    //    CurrentIntensity = CurrentIntensity + IntensitySwitchSpeed;
    //    Color temp = Color.white * CurrentIntensity;
    //    LocalRenderer.material.SetColor("_EmissionColor", temp);
    //    yield return new WaitForSeconds(Timer * Time.deltaTime);
    //}
    //IEnumerator TimerDelayEnd()
    //{
    //    CurrentIntensity = CurrentIntensity - IntensitySwitchSpeed;
    //    Color temp = Color.white * CurrentIntensity;
    //    LocalRenderer.material.SetColor("_EmissionColor", temp);
    //    yield return new WaitForSeconds(Timer * Time.deltaTime);
    //}



}

